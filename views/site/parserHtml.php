<?php

use miloschuman\highcharts\Highcharts;

/* @var $this yii\web\View */
/* @var $model app\models\Parser*/

$this->title = 'Change of Balance Growth';
?>
<div style="height: 100px"></div>
<?= Highcharts::widget([
    'options' => [
        'title' => ['text' => 'Change of Balance Growth'],
        'subtitle' => ['text' => $model->name . ' (Report of: ' . $model->reportDate . ')'],
        'xAxis' => [
            'type' => 'datetime',
            'categories' => $model->dates,
        ],
        'yAxis' => [
            'title' => ['text' => 'Profit'],
        ],
        'series' => [
            ['name' => 'balance', 'data' => $model->profits],
        ]
    ]
]); ?>