<?php

/* @var $this yii\web\View */
/* @var $model app\models\Parser */

$this->title = 'Upload';
?>
<div style="height: 100px"></div>
<div class="upload-form">

    <h1>Upload file</h1>
    <?php $form = \yii\bootstrap\ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'htmlFile')->fileInput()->label('New Report') ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <?= \yii\helpers\Html::submitButton('Upload', ['class' => 'btn btn-sm btn-primary']) ?>
            </div>
        </div>
    </div>

    <?php \yii\bootstrap\ActiveForm::end(); ?>

</div>