<?php

namespace app\models;

require_once dirname(__DIR__) . '/vendor/simplehtmldom/simple_html_dom.php';

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

/**
 * Class Parser
 * @package app\models
 */
class Parser extends Model
{
    public $name;
    public $reportDate;
    public $balance;
    public $profits = [];
    public $dates = [];
    public $htmlFile;

    const FILE_PATH = 'uploads/report.html';

    /**
     * @return array validation rules
     */
    public function rules()
    {
        return [
            [['htmlFile'], 'file', 'maxSize' => 1024 * 1024, 'extensions' => 'html',],
        ];
    }

    /**
     * Gets the html content of the file
     */
    public function getContent()
    {
        if (is_file(Yii::getAlias('@app/public_html/' . self::FILE_PATH))) {
            $this->htmlFile = file_get_html(Yii::getAlias('@app/public_html/' . self::FILE_PATH));
        } else {
            Yii::$app->session->setFlash('error', 'File not found. Please upload the file.');
        }
    }

    /**
     * Upload html file
     * @return bool
     */
    public function uploadFile() : bool
    {
        if ($this->htmlFile = UploadedFile::getInstance($this, 'htmlFile')) {
            if (is_file(Yii::getAlias('@app/public_html/' . self::FILE_PATH))) {
                unlink(Yii::getAlias('@app/public_html/' . self::FILE_PATH));
            }
            if ($this->htmlFile->saveAs(self::FILE_PATH)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Processing data from HTML file
     */
    public function parse()
    {
        if (is_file(Yii::getAlias('@app/public_html/' . self::FILE_PATH))) {
            $dom = file_get_html(Yii::getAlias('@app/public_html/' . self::FILE_PATH));
            if ($table = $dom->find('table', 0)) {
                if ($tr = $table->find('tr', 0)) {
                    if ($tr->find('td', 1) && $tr->find('td', 4)) {
                        $this->retrieveName($tr->find('td', 1)->plaintext);
                        $this->formatDate($tr->find('td', 4)->plaintext);
                    }
                }
            }
            if ($dom->find('tr')) {
                foreach ($dom->find('tr') as $tr) {
                    $balance = $tr->find('td', 4);
                    $profit = $tr->find('td', 13);
                    $date = $tr->find('td', 1);
                    if (
                        $date &&
                        (
                            $this->validateDate($date->plaintext) ||
                            $this->validateDate($date->plaintext, 'Y.m.d H:i')
                        )
                    ) {
                        if ($balance && $this->validateBalance($balance->plaintext)) {
                            $this->balance += str_replace(' ', '', $balance->plaintext);
                            $this->profits[] = $this->balance;
                            $this->dates[] = $date->plaintext;
                        }
                        if ($profit && $this->validateProfit($profit->plaintext)) {
                            $this->balance += str_replace(' ', '', $profit->plaintext);
                            $this->profits[] = $this->balance;
                            $this->dates[] = $date->plaintext;
                        }
                    }
                }
                if (empty($this->dates) || empty($this->profits)) {
                    Yii::$app->session->setFlash('error', 'Data not found. Most likely, the file is incorrect.');
                }
            } else {
                Yii::$app->session->setFlash('error', 'File incorrect.');
            }
        } else {
            Yii::$app->session->setFlash('error', 'File not found. Please upload the file.');
        }
    }

    /**
     * Check the date on the format
     * @param string $date
     * @param string $format
     * @return bool
     */
    private function validateDate(string $date, string $format = 'Y.m.d H:i:s') : bool
    {
        $d = \DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }

    /**
     * Checks the correctness of the cast to float $profit
     * @param string $profit
     * @return bool
     */
    private function validateProfit(string $profit) : bool
    {
        return $profit == (float)$profit;
    }

    /**
     * Checks the correctness of the cast to float $balance
     * @param string $balance
     * @return bool
     */
    private function validateBalance(string $balance) : bool
    {
        return $balance == (float)$balance && 0 != (float)$balance;
    }

    /**
     * Retrieves the name from the string
     * @param string $name the processed string
     * @return bool
     */
    private function retrieveName(string $name) : bool
    {
        if (!$name) {
            return false;
        }
        $this->name = str_replace('Name: ', '', $name);
        return true;
    }

    /**
     * Formats the date
     * @param string $date
     * @param string $oldFormat
     * @param string $newFormat
     * @return bool
     */
    private function formatDate(string $date, string $oldFormat = 'Y M j, H:i', string $newFormat = 'Y.m.d H:i') : bool
    {
        if (!$date) {
            return false;
        }
        $this->reportDate = \DateTime::createFromFormat($oldFormat, $date)->format($newFormat);
        return true;
    }
}
